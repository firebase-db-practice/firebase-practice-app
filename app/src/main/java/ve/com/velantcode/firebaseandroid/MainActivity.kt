package ve.com.velantcode.firebaseandroid

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.floatingactionbutton.FloatingActionButton
import ve.com.velantcode.firebaseandroid.fragment.MyContactsFragment


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val fab: FloatingActionButton = findViewById(R.id.btn_add_contact)
        // Begin the transaction

        val fm = supportFragmentManager.beginTransaction()
        fm.replace(R.id.container,
            MyContactsFragment(), "MyContactsFragment")
        fm.commit()

        fab.setOnClickListener {
            val intent = Intent(this, ContactAddActivity::class.java)
            startActivity(intent)
        }
    }
}
