package ve.com.velantcode.firebaseandroid

import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.database.*
import ve.com.velantcode.firebaseandroid.functions.Validations
import ve.com.velantcode.firebaseandroid.models.Contact
import java.util.*


class ContactAddActivity : AppCompatActivity() {
    // [START define_database_reference]
    private lateinit var database: DatabaseReference
    // [END define_database_reference]
    private val TAG = "ContactAddActivity"

    private val validations = Validations()

    private lateinit var actionBar: ActionBar
    private lateinit var firstNameInput: EditText
    private lateinit var lastNameInput: EditText
    private lateinit var emailInput: EditText
    private lateinit var phoneInput: EditText
    private lateinit var btnAdd: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_contact_add)
        actionBar = supportActionBar!!
        actionBar.setDisplayHomeAsUpEnabled(true)

        // [START create_database_reference]
        database = FirebaseDatabase.getInstance().getReference("contacts")
        // [END create_database_reference]

        firstNameInput = findViewById(R.id.add_firstname)
        lastNameInput = findViewById(R.id.add_lastname)
        emailInput = findViewById(R.id.add_email)
        phoneInput = findViewById(R.id.add_phone)
        btnAdd = findViewById(R.id.add_btn)

        firstNameInput.requestFocus()

        btnAdd.setOnClickListener {
            if (validate()){

                val builder = AlertDialog.Builder(this)
                builder.setTitle("Add contact")
                builder.setMessage("Do you want add this contact?")
                builder.setPositiveButton("Yes!") { _, _ ->
                    addIfNotExistAPhoneNumber()
                }
                builder.setNegativeButton("Cancel") { _, _ ->
                    //
                }

                // Show
                builder.show()
            }
            else {
                Toast.makeText(applicationContext, "You must complete the required fields.", Toast.LENGTH_LONG).show()
            }

        }
    }

    // =============================================================================================

    private fun clearInputs() {
        firstNameInput.setText("")
        lastNameInput.setText("")
        emailInput.setText("")
        phoneInput.setText("")
        firstNameInput.error = null
        lastNameInput.error = null
        emailInput.error = null
        phoneInput.error = null
    }

    private fun validate(): Boolean {
        val firstName = firstNameInput.text.toString()
        val lastName = lastNameInput.text.toString()
        val email = emailInput.text.toString()
        val phone = phoneInput.text.toString()
        var ret = true;

        if (firstName == "" || !validations.checkNameOrLastName(firstName)){

            firstNameInput.error = "Required"
            ret = false
        }
        else firstNameInput.error = null

        if (lastName == "" || !validations.checkNameOrLastName(lastName)){
            lastNameInput.error = "Required"
            ret = false
        }
        else lastNameInput.error = null

        if (phone == "" || !validations.checkNumberPhone(phone)){
            phoneInput.error = "Required"
            ret = false
        }
        else phoneInput.error = null

        if (email != "" && !validations.checkEmail(email)){
            emailInput.error = "Insert a correct e-mail."
            ret = false
        }
        else emailInput.error = null

        return ret

    }

    private fun addIfNotExistAPhoneNumber() {

        database.child("phone").equalTo(phoneInput.text.toString())

        val ev = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                // check if the new phone number exist in other contact
                val exist = checkIfNumberExist(dataSnapshot.children)

                if (exist){
                    phoneInput.error = "The phone number already exists."
                }
                else {
                    phoneInput.error = null
                    // set data
                    val contact = Contact()
                    contact.setFirstName(firstNameInput.text.toString())
                    contact.setLastName(lastNameInput.text.toString())
                    contact.setPhone(phoneInput.text.toString())
                    contact.setEmail(emailInput.text.toString())
                    val uuid = UUID.randomUUID() // generate uuid

                    // save contact
                    database.child(uuid.toString()).setValue(contact)

                    // clear inputs
                    clearInputs()

                    Toast.makeText(applicationContext, "Contact was successful added.", Toast.LENGTH_LONG).show()
                    onBackPressed()
                }
            }

            override fun onCancelled(databaseError: DatabaseError) {
                // Getting Contacts failed, log a message
                Log.w(TAG, "loadPost:onCancelled", databaseError.toException())
                Toast.makeText(applicationContext, "Error get contact data.", Toast.LENGTH_LONG).show()
                onBackPressed()
            }
        }
        database.addListenerForSingleValueEvent(ev)
    }

    private fun checkIfNumberExist(children: MutableIterable<DataSnapshot>) : Boolean {
        children.forEach {
            if (it.child("phone").value.toString() == phoneInput.text.toString()){
                return true
            }
        }

        return false;
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        onBackPressed()
        return true
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }
}
