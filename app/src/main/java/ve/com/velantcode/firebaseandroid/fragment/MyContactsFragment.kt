package ve.com.velantcode.firebaseandroid.fragment

import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.Query

class MyContactsFragment : ContactFragment() {

    override fun getQuery(databaseReference: DatabaseReference): Query {
        // All my contacts
        return databaseReference.child("contacts").orderByChild("firstname")
    }

}