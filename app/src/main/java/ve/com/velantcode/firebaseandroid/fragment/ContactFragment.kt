package ve.com.velantcode.firebaseandroid.fragment

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.firebase.ui.database.FirebaseRecyclerAdapter
import com.firebase.ui.database.FirebaseRecyclerOptions
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.model_contact_list.view.*
import ve.com.velantcode.firebaseandroid.ContactDetailsActivity
import ve.com.velantcode.firebaseandroid.holders.ContactHolder
import ve.com.velantcode.firebaseandroid.R
import ve.com.velantcode.firebaseandroid.models.Contact

abstract class ContactFragment : Fragment() {

    // [START define_database_reference]
    private lateinit var database: DatabaseReference
    // [END define_database_reference]

    private val TAG = "MainActivity"
    private lateinit var ctx: Context
    private lateinit var recycler: RecyclerView
    private lateinit var msgContacts: TextView
    private lateinit var manager: LinearLayoutManager
    private var adapter: FirebaseRecyclerAdapter<Contact, ContactHolder>? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val rootView = inflater.inflate(R.layout.fragment_main, container, false)
        ctx = container!!.context

        // [START create_database_reference]
        database = FirebaseDatabase.getInstance().reference
        // [END create_database_reference]

        msgContacts = rootView.findViewById(R.id.msg_contacts)
        recycler = rootView.findViewById(R.id.contact_list)
        recycler.setHasFixedSize(true)

        // Inflate the layout for this fragment
        return rootView
    }

    @SuppressLint("SetTextI18n")
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        // Set up Layout Manager, reverse layout
        manager = LinearLayoutManager(activity)
        manager.reverseLayout = true
        manager.stackFromEnd = true
        recycler.layoutManager = manager

        // Set up FirebaseRecyclerAdapter with the Query
        val contactsQuery = getQuery(database)

        msgContacts.text = "Loading …"

        val options = FirebaseRecyclerOptions.Builder<Contact>()
            .setQuery(contactsQuery, Contact::class.java)
            .build()

        // [start post_value_event_listener]
        val contactListener = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                // check if exist data
                if (options.snapshots.size == 0){
                    recycler.visibility = View.GONE
                    msgContacts.visibility = View.VISIBLE
                    msgContacts.text = "There are no registered contacts."
                }
                else {
                    msgContacts.visibility = View.GONE
                    recycler.visibility = View.VISIBLE
                    msgContacts.text = ""
                }

            }

            override fun onCancelled(databaseError: DatabaseError) {
                // Getting Contacts failed, log a message
                Log.w(TAG, "loadPost:onCancelled", databaseError.toException())
                recycler.visibility = View.GONE
                msgContacts.visibility = View.VISIBLE
                msgContacts.text = "There are no registered contacts."
            }
        }
        database.addValueEventListener(contactListener)
        // [END post_value_event_listener]

        adapter = object : FirebaseRecyclerAdapter<Contact, ContactHolder>(options) {

            override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ContactHolder {
                val v = LayoutInflater.from(viewGroup.context).inflate(R.layout.model_contact_list, viewGroup, false)
                val lp = RecyclerView.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT
                )
                lp.topMargin = 5
                v.layoutParams = lp
                return ContactHolder(v)
            }

            override fun onBindViewHolder(viewHolder: ContactHolder, position: Int, contact: Contact) {
                val contactRef = getRef(position)

                // Set click listener for the whole post view
                val contactKey = contactRef.key
                viewHolder.bindToContact(contact)

                viewHolder.itemView.setOnClickListener {
                    val intent = Intent(activity, ContactDetailsActivity::class.java)
                    intent.putExtra("contactKey", contactKey)
                    startActivity(intent)
                }

                viewHolder.itemView.btn_contact_delete.setOnClickListener {
                    val builder = AlertDialog.Builder(ctx)
                    builder.setTitle("Delete contact")
                    builder.setMessage("Are you sure that you want delete this contact?")
                    builder.setPositiveButton("Yes!") { _, _ ->
                        database.child("contacts/${contactKey}").removeValue()
                        Toast.makeText(activity, "The contact was successful removed.", Toast.LENGTH_SHORT).show()
                    }
                    builder.setNegativeButton("Cancel") { _, _ ->
                        //
                    }

                    // Show
                    builder.show()
                }
            }
        }
        recycler.adapter = adapter
    }

    override fun onStart() {
        super.onStart()
        adapter?.startListening()
    }

    override fun onDestroy() {
        super.onDestroy()
        adapter?.stopListening()
    }

    abstract fun getQuery(databaseReference: DatabaseReference): Query
}
