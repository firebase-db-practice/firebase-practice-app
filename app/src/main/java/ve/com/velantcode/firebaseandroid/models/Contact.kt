package ve.com.velantcode.firebaseandroid.models

class Contact () {

    private var firstname: String = ""
    private var lastname: String = ""
    private var email: String = ""
    private var phone: String = ""

    fun getFirstName(): String {
        return firstname
    }

    fun setFirstName(firstname: String) {
        this.firstname = firstname
    }

    fun getLastName(): String {
        return lastname
    }

    fun setLastName(lastname: String) {
        this.lastname = lastname
    }

    fun getEmail(): String {
        return email
    }

    fun setEmail(email: String) {
        this.email = email
    }

    fun getPhone(): String {
        return phone
    }

    fun setPhone(phone: String) {
        this.phone = phone
    }

}