package ve.com.velantcode.firebaseandroid

import android.app.Activity
import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AlertDialog
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.firebase.database.*
import ve.com.velantcode.firebaseandroid.models.Contact

class ContactDetailsActivity : AppCompatActivity() {
    // [START define_database_reference]
    private lateinit var database: DatabaseReference
    private var contactListener: ValueEventListener? = null
    // [END define_database_reference]

    private val TAG = "ContactDetailsActivity"

    private lateinit var fab: FloatingActionButton
    private lateinit var actionBar: ActionBar
    private lateinit var linearDetails: LinearLayout
    private lateinit var linearEdit: LinearLayout
    private lateinit var firstNameTxt: TextView
    private lateinit var lastNameTxt: TextView
    private lateinit var emailTxt: TextView
    private lateinit var phoneTxt: TextView
    private lateinit var emailTitleTxt: TextView
    private lateinit var firstNameInput: EditText
    private lateinit var lastNameInput: EditText
    private lateinit var emailInput: EditText
    private lateinit var phoneInput: EditText
    private lateinit var btnUpdate: Button
    private var showEdit = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_contact_details)
        actionBar = supportActionBar!!
        actionBar.setDisplayHomeAsUpEnabled(true)

        fab = findViewById(R.id.fab_edit)

        linearDetails = findViewById(R.id.linear_details)
        linearEdit = findViewById(R.id.linear_edit)
        firstNameTxt = findViewById(R.id.firstname_txt)
        lastNameTxt = findViewById(R.id.lastname_txt)
        emailTxt = findViewById(R.id.email_txt)
        emailTitleTxt = findViewById(R.id.email_title_txt)
        phoneTxt = findViewById(R.id.phone_txt)
        firstNameInput = findViewById(R.id.edit_input_firstname)
        lastNameInput = findViewById(R.id.edit_input_lastname)
        emailInput = findViewById(R.id.edit_input_email)
        phoneInput = findViewById(R.id.edit_input_phone)
        btnUpdate = findViewById(R.id.btn_update)

        val args = this.intent.extras!!

        if (args.getString("contactKey").isNullOrBlank()){
            Toast.makeText(applicationContext, "Error get contact key.", Toast.LENGTH_LONG).show()
            onBackPressed()
        }

        val key = args.getString("contactKey")!!

        // [START create_database_reference]
        database = FirebaseDatabase.getInstance().reference.child("contacts/${key}")
        // [END create_database_reference]

        fab.setOnClickListener {
            showEditForm(showEdit)
        }

        btnUpdate.setOnClickListener {

            val builder = AlertDialog.Builder(this)
            builder.setTitle("Update contact")
            builder.setMessage("Are you sure that you want update contact data?")
            builder.setPositiveButton("Yes!") { _, _ ->
                updateContact()
            }
            builder.setNegativeButton("Cancel") { _, _ ->
                //
            }

            // Show
            builder.show()
        }
    }

    override fun onStart() {
        super.onStart()

        // [start post_value_event_listener]
        val contactListener = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                // Get Post object and use the values to update the UI
                val contact = dataSnapshot.getValue(Contact::class.java)

                if (contact == null) {
                    Toast.makeText(applicationContext, "Error get contact data.", Toast.LENGTH_LONG).show()
                    finish()
                    return
                }

                contact.let {
                    // set details data
                    firstNameTxt.text = it.getFirstName()
                    lastNameTxt.text = it.getLastName()
                    phoneTxt.text = it.getPhone()
                    if (it.getEmail().isNotBlank()) {
                        emailTxt.text = it.getEmail()
                        emailTxt.visibility = View.VISIBLE
                        emailTitleTxt.visibility = View.VISIBLE
                    }
                    else {
                        emailTxt.visibility = View.GONE
                        emailTitleTxt.visibility = View.GONE
                    }

                    // change data input
                    firstNameInput.setText(it.getFirstName())
                    lastNameInput.setText(it.getLastName())
                    phoneInput.setText(it.getPhone())
                    emailInput.setText(it.getEmail() ?: "")
                }

            }

            override fun onCancelled(databaseError: DatabaseError) {
                // Getting Contacts failed, log a message
                Log.w(TAG, "loadPost:onCancelled", databaseError.toException())
                Toast.makeText(applicationContext, "Error get contact data.", Toast.LENGTH_LONG).show()
                onBackPressed()
            }
        }
        database.addValueEventListener(contactListener)
        // [END post_value_event_listener]

        // Keep copy of post listener so we can remove it when app stops
        this.contactListener = contactListener
    }

    public override fun onStop() {
        super.onStop()

        // Remove post value event listener
        contactListener?.let {
            database.removeEventListener(it)
        }
    }

    // =============================================================================================

    private fun updateContact(){
        val contact = Contact()
        contact.setFirstName(firstNameInput.text.toString().trim())
        contact.setLastName(lastNameInput.text.toString().trim())
        contact.setEmail(emailInput.text.toString().trim())
        contact.setPhone(phoneInput.text.toString().trim())

        database.setValue(contact)

        Toast.makeText(applicationContext, "Contact was successful updated.", Toast.LENGTH_LONG).show()
        showEditForm(false)
    }

    private fun showEditForm(show: Boolean){
        if (show) {
            linearDetails.visibility = View.GONE
            linearEdit.visibility = View.VISIBLE
            fab.setImageResource(android.R.drawable.ic_menu_close_clear_cancel)
            showEdit = false
            actionBar.title = "Edit contact"
        }
        else {
            hideKeyboard(View(this))
            linearEdit.visibility = View.GONE
            linearDetails.visibility = View.VISIBLE
            actionBar.title = "Details contact"
            fab.setImageResource(android.R.drawable.ic_menu_edit)
            showEdit = true
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (!showEdit) {
            showEditForm(false)
        }
        else {
            finish()
        }
        return true
    }

    override fun onBackPressed() {
        super.onBackPressed()

        if (!showEdit) {
            showEditForm(false)
        }
        else {
            finish()
        }
    }

    private fun Context.hideKeyboard(view: View) {
        val inputMethodManager = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
    }
}
