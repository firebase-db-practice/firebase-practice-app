package ve.com.velantcode.firebaseandroid.functions

import android.text.TextUtils
import android.util.Patterns
import java.util.regex.Matcher
import java.util.regex.Pattern

class Validations {

    fun checkEmail(email: String): Boolean {
        return !TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }

    fun checkNameOrLastName(value: String) : Boolean {
        val p: Pattern = Pattern.compile("^([A-Za-z ÁÉÍÓÚÀÈÌÒÙàèìòùáéíóúÂÊÎÔÛâêîôûÄËÏÖÜäëïöüñÑ]{2,40})$")
        val m: Matcher = p.matcher(value)

        return m.matches()
    }

    fun checkNumberPhone(value: String) : Boolean {
        val p: Pattern = Pattern.compile("^[\\+]?[(]?[0-9]{3}[)]?[-\\s\\.]?[0-9]{3}[-\\s\\.]?[0-9]{4,6}$")
        val m: Matcher = p.matcher(value)

        return m.matches()
    }
}