package ve.com.velantcode.firebaseandroid.holders

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.model_contact_list.view.*
import ve.com.velantcode.firebaseandroid.models.Contact

class ContactHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {

    fun bindToContact(contact: Contact) {
        val fullName = contact.getFirstName() + " " + contact.getLastName()
        itemView.model_contact_names.text = fullName
        itemView.model_contact_phone.text = contact.getPhone()
        itemView.model_contact_phone.text = contact.getPhone()
        if (contact.getEmail().isNotBlank()) {
            itemView.model_contact_email.text = contact.getEmail()
            itemView.model_contact_email.visibility = View.VISIBLE
        }
        else {
            itemView.model_contact_email.visibility = View.GONE
        }
    }

    override fun onClick(v: View?) {
        //
    }
}