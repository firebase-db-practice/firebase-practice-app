# Firebase Database - API Practice - Contact's Directory.

[![pipeline status](https://gitlab.com/firebase-db-practice/firebase-practice-app/badges/master/pipeline.svg)](https://gitlab.com/firebase-db-practice/firebase-practice-app/commits/master)

This is a simple practice to use `Firebase Realtime Database` in Android with Kotlin. This app is a Contact's Directory, you can add, view, edit and delete contacts. 
Download and practice. Read and modify the code to functionality understand. 
You must follow the steps (down) to init de app.
Enjoy.

## Step required

* Clone this repository: `git clone git@gitlab.com:firebase-db-practice/firebase-practice-app.git`
* Init Android Studio and sync gradle.
* Config the project with Firebase. In this [Link](https://firebase.google.com/docs/android/setup?authuser=0) you will find the steps. 
* Once project configured, you can run the app in the ADV or your device.

## Documentation 

#### Firebase integration in Android
Official documentation: [Firebase Android](https://firebase.google.com/docs/android/setup?authuser=0).

#### Firebase Database Documentation
Official documentation: [Firebase Database to Android Kotlin](https://firebase.google.com/docs/reference/kotlin/com/google/firebase/database/package-summary?authuser=0).
